/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Log from '../../../../../../../../common/src/main/ets/default/Log.ets'
import FeatureAbilityManager from '../../../../../../../../common/src/main/ets/default/abilitymanager/featureAbilityManager.ets'
import WindowManager from '../../../../../../../../common/src/main/ets/default/WindowManager.ets'
import ViewModel from '../viewmodel/LocationVM.ets'
import SimpleToggleBase from '../../../../../../../../common/src/main/ets/template/SimpleToggleBase.ets'

const TAG = 'location-ControlCenterSimpleToggleLocationComponent'

@Component
export default struct ControlCenterSimpleToggleLocationComponent {
  @Prop keyId: string
  @Prop mShowLabel: boolean
  @Prop mEditMode: boolean
  @State mIcon: Resource = $r("app.media.ic_controlcenter_gps")
  @State mLabel: Resource = $r("app.string.control_center_complex_toggle_location_title")
  @StorageLink('LocationServiceOpenStatus') LocationServiceOpenStatus: boolean = false
  @StorageLink("showStatusBar") showStatusBar: boolean = false
  private mWindowManager
  private mFeatureAbilityManager

  aboutToAppear() {
    Log.showInfo(TAG, 'aboutToAppear')
    this.mWindowManager = new WindowManager()
    this.mFeatureAbilityManager = new FeatureAbilityManager()
    ViewModel.initViewModel()
  }

  aboutToDisappear() {
    Log.showInfo(TAG, 'aboutToDisappear')
  }

  build() {
    SimpleToggleBase({
      mToggleId: this.keyId,
      mIcon: $mIcon,
      mAutoIconColor: true,
      mChangeSwitch: $LocationServiceOpenStatus,
      mLabel: $mLabel,
      mShowLabel: this.mShowLabel,
      mEditMode: this.mEditMode,
      mClickEvent: this.mClickEvent.bind(this),
      mLongClickEvent: this.mLongClickEvent.bind(this)
    })
  }

  mClickEvent() {
    Log.showInfo(TAG, `mClickEvent, LocationServiceOpenStatus: ${this.LocationServiceOpenStatus}`)
    if (this.LocationServiceOpenStatus) {
      ViewModel.disableLocation()
    } else {
      ViewModel.enableLocation()
    }
  }

  mLongClickEvent() {
    Log.showInfo(TAG, `mLongClickEvent, LocationServiceOpenStatus: ${this.LocationServiceOpenStatus}`)

    this.showStatusBar = true
    this.mWindowManager.setWindowMin((result) => {
      this.mFeatureAbilityManager.openAbility(TAG, {
        want: {
          bundleName: 'com.ohos.settings',
          abilityName: 'com.ohos.settings.MainAbility',
        }
      })
    })

  }
}